//
//  TCSAppDelegate.h
//  Bleu Auto
//
//  Created by Tim Perfitt on 4/12/14.
//  Copyright (c) 2014 Twocanoes Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
