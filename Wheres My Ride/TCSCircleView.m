//
//  TCSCircleView.m
//  Bleu Auto
//
//  Created by Tim Perfitt on 4/13/14.
//  Copyright (c) 2014 Twocanoes Software, Inc. All rights reserved.
//

#import "TCSCircleView.h"

@implementation TCSCircleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
       
    int radius=self.bounds.size.width/2;
    CGRect circleRect=CGRectMake(self.center.x, self.center.y, radius*2, radius*2);
    
    circleRect=CGRectOffset(circleRect, -radius, -radius);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // Set the border width
    CGContextSetLineWidth(contextRef, 1.0);
    
    // Set the circle fill color to GREEN
    CGContextSetRGBFillColor(contextRef, 0.0, 255.0, 0.0, 1.0);
    
    // Set the cicle border color to BLUE
    CGContextSetRGBStrokeColor(contextRef, 0.0, 0.0, 255.0, 1.0);
    
    // Fill the circle with the fill color
    CGContextFillEllipseInRect(contextRef, circleRect);
    
    // Draw the circle border
    CGContextStrokeEllipseInRect(contextRef, circleRect);
    
}


@end
