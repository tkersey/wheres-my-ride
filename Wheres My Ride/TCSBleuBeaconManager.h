//
//  TCSBleuURLMapper.h
//  Bleu
//
//  Copyright (c) 2013 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
/*!
 This class is a singleton that serves as a delegate to CoreLocation and sends
 out notificatios as location events arrive. CoreLocation callbacks have to occurr
 on the main thread. This class has some guards to make that happen, but it is
 not completely debugged. If you have problems with receiving notifications,
 check whether you are causing a delegate to fire on a background thread.
 */
typedef NS_ENUM(NSUInteger, TCSProximityIndex){
    kImmediate = 0,
    kNear,
    kFar,
	kDefault
};

/*!
 Notification constants. The TCSBleuBeaconManager registeres as the delegate
 to CoreLocation, and turns delegate callbacks into notifications.
 
TCSBleuRangingNotification is called while the device is in the beacon region
 and changes proximity.
 */
extern NSString * const TCSDidEnterBleuRegionNotification;
extern NSString * const TCSDidExitBleuRegionNotification;
extern NSString * const TCSBleuRangingNotification;

@interface TCSBleuBeaconManager : NSObject <CLLocationManagerDelegate>
@property (strong, readonly) NSString *defaultURL;
@property (strong, nonatomic) NSString *entryText;
@property (strong, readonly) NSString *exitText;
@property (strong, nonatomic) NSString *proximityUUID;
@property (assign, nonatomic) int major;
@property (assign, nonatomic) int minor;
@property (assign  ,nonatomic) CLLocationCoordinate2D savedLocation;

+ (instancetype) sharedManager;

- (void)beginRegionRanging;
-(void)stopRegionRanging;
-(void)startUpdatingLocation;
@end
