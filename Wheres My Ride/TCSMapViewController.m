//
//  TCSMapViewController.m
//  Bleu Auto
//
//  Created by Tim Perfitt on 4/16/14.
//  Copyright (c) 2014 Twocanoes Software, Inc. All rights reserved.
//

#import "TCSMapViewController.h"

@interface TCSMapViewController ()

@end

@implementation TCSMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [self.mapView setZoomEnabled:YES];
    self.mapView.showsUserLocation=YES;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.002;
    span.longitudeDelta = 0.002;
    MKCoordinateRegion region;
    region.center=self.carLocation;
    region.span=span;
    [self.mapView setRegion:region];
    
    MKPointAnnotation *myPersonalAnnotation= [[MKPointAnnotation alloc] init];
    myPersonalAnnotation.title= @"Car";
    myPersonalAnnotation.coordinate=self.carLocation;
    MKPinAnnotationView *myPersonalView=[[MKPinAnnotationView alloc] initWithAnnotation:myPersonalAnnotation reuseIdentifier:@"car"];
    myPersonalView.pinColor=MKPinAnnotationColorPurple;
    [self.mapView addAnnotation:myPersonalAnnotation];
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
